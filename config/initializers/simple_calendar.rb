module SimpleCalendar
  class Calendar

    def render_week(week)
      results = week.map do |day|
        content_tag :td, get_option(:td, start_date, day) do
          content_tag :div, class: "day-wrapper" do
            block.call(day.strftime("%d"), events_for_date(day))
          end
        end
      end
      safe_join results
    end

  end
end
