# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  # Wrappers are used by the form builder to generate a
  # complete input. You can remove any component from the
  # wrapper, change the order or even add your own to the
  # stack. The options given below are used to wrap the
  # whole input.

  config.wrappers :amnat, :class => :input, :hint_class => :hints, :error_class => :errors do |b|

    b.use :html5
    # Calculates placeholders automatically from I18n
    # You can also pass a string as f.input :placeholder => "Placeholder"
    b.use :placeholder

    b.use :maxlength
    b.use :pattern
    b.use :min_max
    b.use :readonly

    b.wrapper :tag => "div", :class => "label" do |bl|
      bl.use :label
      bl.use :hint,  :wrap_with => { :tag => :span, :class => :hint }
    end
    b.wrapper :tag => "div", :class => "field" do |bf|
      bf.use :input
      bf.use :error, :wrap_with => { :tag => :span, :class => :error }
    end
  end

  config.wrappers :inline, :class => :input, :hint_class => :hints, :error_class => :errors do |b|

    b.use :html5
    b.use :readonly

    b.wrapper :tag => "div", :class => "label" do |bl|
      bl.use :input
      bl.use :label
      bl.use :error, :wrap_with => { :tag => :span, :class => :error }
      bl.use :hint,  :wrap_with => { :tag => :span, :class => :hint }
    end
  end

  config.wrappers :bootstrap_horizontal, hint_class: :hints, error_class: :errors do |b|

    b.use :html5
    b.use :readonly

    b.wrapper tag: "div", class: "form-group" do |bl|
      bl.use :label, class: "col-sm-2 control-label"
      bl.wrapper tag: "div", class: "col-sm-10" do |blw|
        blw.use :input, class: "form-control"
      end
      bl.use :error, :wrap_with => { :tag => :span, :class => :error }
      bl.use :hint,  :wrap_with => { :tag => :span, :class => :hint }
    end
  end

  # The default wrapper to be used by the FormBuilder.
  config.default_wrapper = :amnat

  # Define the way to render check boxes / radio buttons with labels.
  # Defaults to :nested for bootstrap config.
  #   :inline => input + label
  #   :nested => label > input
  config.boolean_style = :nested

  # Default class for buttons
  config.button_class = 'btn'

  # Method used to tidy up errors.
  # config.error_method = :first

  # Default tag used for error notification helper.
  config.error_notification_tag = :div

  # CSS class to add for error notification helper.
  config.error_notification_class = 'alert alert-error'

  # ID to add for error notification helper.
  # config.error_notification_id = nil

  # Series of attempts to detect a default label method for collection.
  # config.collection_label_methods = [ :to_label, :name, :title, :to_s ]

  # Series of attempts to detect a default value method for collection.
  # config.collection_value_methods = [ :id, :to_s ]

  # You can wrap a collection of radio/check boxes in a pre-defined tag, defaulting to none.
  # config.collection_wrapper_tag = nil

  # You can define the class to use on all collection wrappers. Defaulting to none.
  # config.collection_wrapper_class = nil

  # You can wrap each item in a collection of radio/check boxes with a tag,
  # defaulting to :span. Please note that when using :boolean_style = :nested,
  # SimpleForm will force this option to be a label.
  # config.item_wrapper_tag = :span

  # You can define a class to use in all item wrappers. Defaulting to none.
  # config.item_wrapper_class = nil

  # How the label text should be generated altogether with the required text.
  config.label_text = lambda { |label, required, explicit_label| "#{required} #{label}" }

  # You can define the class to use on all labels. Default is nil.
  # config.label_class = 'control-label'

  # You can define the class to use on all forms. Default is simple_form.
  # config.form_class = :simple_form

  # You can define which elements should obtain additional classes
  # config.generate_additional_classes_for = [:wrapper, :label, :input]

  # Whether attributes are required by default (or not). Default is true.
  # config.required_by_default = true

  # Tell browsers whether to use default HTML5 validations (novalidate option).
  # Default is enabled.
  config.browser_validations = false

  # Collection of methods to detect if a file type was given.
  # config.file_methods = [ :mounted_as, :file?, :public_filename ]

  # Custom mappings for input types. This should be a hash containing a regexp
  # to match as key, and the input type that will be used when the field name
  # matches the regexp as value.
  # config.input_mappings = { /count/ => :integer }

  # Default priority for time_zone inputs.
  # config.time_zone_priority = nil

  # Default priority for country inputs.
  # config.country_priority = nil

  # Default size for text inputs.
  # config.default_input_size = 50

  # When false, do not use translations for labels.
  # config.translate_labels = true

  # Automatically discover new inputs in Rails' autoload path.
  # config.inputs_discovery = true

  # Cache SimpleForm inputs discovery
  # config.cache_discovery = !Rails.env.development?
end
