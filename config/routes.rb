Rails.application.routes.draw do

  localized do

    root 'home#index'

    devise_for :users, controllers: { registrations: 'registrations' }

    resource :contact
    resource :team
    resource :media

    resources :groups do
      post :order, on: :collection
    end
    resources :coaches
    resources :news_posts
    resources :fighters
    resources :trainings

    resources :passes
    resources :videos, only: [:index, :new, :create, :destroy]
    resources :slides
    resources :comments, only: [:new, :create, :destroy]
    resources :events
    resources :messages do
      post :read, on: :member
    end
    resources :faqs
    resources :members do
      resources :payments
    end
    resources :meetings
    resources :personal_trainings, only: :index
    resources :users do
      resources :meetings, only: :index, controller: 'users/meetings' do
        post :sign_up, on: :member
        delete :cancel, on: :member
      end
    end

    resources :partners, only: [:new, :create, :destroy] do
      post :order, on: :collection
    end
    resources :galleries do
      post :order, on: :collection
    end

    resources :pages, only: [:index, :new, :edit, :create] do
      post :order, on: :collection
      resources :page_contents, except: :show do
        post :order, on: :collection
      end
    end

    root "home#index"

    put "/:id", to: "pages#update"
    patch "/:id", to: "pages#update"
    delete "/:id", to: "pages#destroy"
    get "/:id", to: "pages#show", constraints: { id: /(?!.*(en|ru)).*/ }, as: :page
  
  end
end
