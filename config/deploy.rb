set :repo_url, "git@bitbucket.org:wojcieszka/amnat.git"

set :tmp_dir, "/home/amnat/www/tmp"

role :app, %w{amnat@amnat.megiteam.pl}
role :web, %w{amnat@amnat.megiteam.pl}
role :db,  %w{amnat@amnat.megiteam.pl}

set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }

# set :format, :pretty
set :log_level, :info
# set :pty, true

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{log tmp public/uploads}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 5

namespace :deploy do

  desc "Restart application"
  task :restart do
    on roles(:app) do
      execute "appctl restart #{fetch(:application)}"
    end
  end

  after :finishing, "deploy:cleanup"

  namespace :static_error_pages do
    task :generate do
      on roles(:web) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, "generate_static_error_pages"
          end
        end
      end
    end
  end
end

after "deploy:publishing", "deploy:restart"
# after  "deploy:restart", "deploy:static_error_pages:generate"
