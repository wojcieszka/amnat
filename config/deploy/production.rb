set :application, "amnat"

set :stage, :production
set :branch, :master
set :deploy_to, "/home/amnat/www/application"

set :rvm_ruby_version, "2.0.0@amnat"