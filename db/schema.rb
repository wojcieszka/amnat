# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_20_174446) do

  create_table "coach_translations", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "coach_id", null: false
    t.string "locale", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "description"
    t.index ["coach_id"], name: "index_coach_translations_on_coach_id"
    t.index ["locale"], name: "index_coach_translations_on_locale"
  end

  create_table "coaches", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "image"
    t.float "personal_training_price"
  end

  create_table "comments", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "news_post_id"
    t.string "author"
    t.text "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.string "city"
    t.string "venue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "description"
    t.datetime "starts_at"
    t.string "slug"
    t.string "image"
  end

  create_table "faqs", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "question"
    t.text "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fighters", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "galleries", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "position"
    t.string "short_title"
  end

  create_table "gallery_images", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "image"
    t.integer "gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.integer "number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "position"
    t.string "level"
  end

  create_table "meetings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "training_id"
    t.datetime "event_at"
    t.index ["training_id", "event_at"], name: "index_meetings_on_training_id_and_event_at", unique: true
  end

  create_table "members", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "token"
    t.string "first_name"
    t.string "last_name"
    t.text "description"
    t.integer "status"
    t.boolean "declaration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "payment_status"
    t.integer "group_id"
    t.string "email"
    t.date "date_of_birth"
    t.string "phone"
  end

  create_table "messages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.string "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "status"
  end

  create_table "news_posts", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "image"
  end

  create_table "page_contents", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "page_id"
    t.string "title"
    t.string "name"
    t.text "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "locale"
  end

  create_table "page_translations", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "page_id", null: false
    t.string "locale", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "title"
    t.string "menu_title"
    t.string "slug"
    t.index ["locale"], name: "index_page_translations_on_locale"
    t.index ["page_id"], name: "index_page_translations_on_page_id"
  end

  create_table "pages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.string "name"
    t.integer "position"
    t.string "menu_title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "parent_id"
    t.integer "menu"
  end

  create_table "partners", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.string "link"
    t.string "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pass_translations", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "pass_id", null: false
    t.string "locale", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "description"
    t.string "name"
    t.index ["locale"], name: "index_pass_translations_on_locale"
    t.index ["pass_id"], name: "index_pass_translations_on_pass_id"
  end

  create_table "passes", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.float "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.datetime "due_date"
    t.integer "member_id"
    t.integer "group_id"
    t.integer "amount"
    t.text "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seos", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.text "keywords"
    t.integer "seoable_id"
    t.string "seoable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "locale"
  end

  create_table "slides", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "position"
  end

  create_table "training_days", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "name"
    t.time "time_start"
    t.time "time_end"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "group_id"
  end

  create_table "user_meetings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.integer "user_id"
    t.integer "meeting_id"
    t.datetime "created_at"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "role"
    t.string "first_name"
    t.string "last_name"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "videos", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci", force: :cascade do |t|
    t.string "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
