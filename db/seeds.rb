Page.create_with(slug: "root", title: "root").find_or_create_by(name: "root")
Page.create_with(slug: "treningi", title: "Treningi", menu: 0, menu_title: "Treningi").find_or_create_by(name: "trainings")
Page.create_with(slug: "grafik", title: "Grafik zajęć", menu: 0, menu_title: "Grafik", parent_id: Page.find_by_name("trainings").id).find_or_create_by(name: "groups")
Page.create_with(slug: "kontakt", title: "Kontakt", menu: 0, menu_title: "Kontakt").find_or_create_by(name: "contact")
Page.create_with(slug: "cennik", title: "Cennik zajęć", menu: 0, menu_title: "Cennik", parent_id: Page.find_by_name("trainings").id).find_or_create_by(name: "passes")
Page.create_with(slug: "kadra", title: "Kadra", menu: 0, menu_title: "Kadra").find_or_create_by(name: "team")
Page.create_with(slug: "zawodnicy", title: "Zawodnicy", menu: 0, menu_title: "Zawodnicy", parent_id: Page.where(name: "team").first.id).find_or_create_by(name: "fighters")
Page.create_with(slug: "instruktorzy", title: "Instruktorzy", menu: 0, menu_title: "Instruktorzy", parent_id: Page.where(name: "team").first.id).find_or_create_by(name: "coaches")
Page.create_with(slug: "aktualnosci", title: "Aktualności", menu: 0, menu_title: "Aktualności").find_or_create_by(name: "news_posts")
Page.create_with(slug: "wydarzenia", title: "Wydarzenia", menu: 1, menu_title: "Wydarzenia").find_or_create_by(name: "events")
Page.create_with(slug: "media", title: "Media", menu: 0, menu_title: "Media").find_or_create_by(name: "media")
Page.create_with(slug: "partnerzy", title: "Partnerzy", menu_title: "Partnerzy").find_or_create_by(name: "partners")
Page.create_with(slug: "galeria", title: "Galeria", menu: 0, menu_title: "Galeria", parent_id: Page.find_by_name("media").id).find_or_create_by(name: "galleries")
Page.create_with(slug: "video", title: "Video", menu: 0, menu_title: "Video", parent_id: Page.find_by_name("media").id).find_or_create_by(name: "videos")
Page.create_with(slug: "uzytkownicy", title: "uzytkownicy", menu_title: "uzytkownicy").find_or_create_by(name: "users")
Page.create_with(slug: "o-nas", title: "O nas", menu: 1, menu_title: "O nas").find_or_create_by(name: "about_us")
Page.create_with(slug: "faq", title: "Najczęściej zadawane pytania", menu: 1, menu_title: "FAQ").find_or_create_by(name: "faqs")
Page.create_with(slug: "muaythai", title: "Muaythai", menu: 0, menu_title: "Muaythai").find_or_create_by(name: "muaythai")
Page.create_with(slug: "treningi-personalne", title: "Treningi personalne", menu: 0, menu_title: "Personalne", parent: Page.find_by_name("trainings")).find_or_create_by(name: "personal_trainings")
Page.create_with(slug: "moje-treningi", title: "Moje treningi", menu_title: "Moje treningi").find_or_create_by(name: "meetings")

%w(pl en ru).each do |lang|
  %w(muaythai about_us fighters).each do |page|
    PageContent.find_or_create_by(page_id: Page.find_by(name: page).id, content: "Zawartość", locale: lang)
  end
end