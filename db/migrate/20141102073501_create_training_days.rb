class CreateTrainingDays < ActiveRecord::Migration
  def change
    create_table :training_days do |t|
      t.string :name
      t.time :time_start
      t.time :time_end

      t.timestamps
    end
  end
end
