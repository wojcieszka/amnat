class RemoveMenuFromPage < ActiveRecord::Migration
  def change
    remove_column :pages, :menu, :string
  end
end
