class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.datetime :date
      t.string :title
      t.string :city
      t.string :venue

      t.timestamps
    end
  end
end
