class RemoveLevelFromGroup < ActiveRecord::Migration
  def change
    remove_column :groups, :level, :string
  end
end
