class AddUniqueIndexToMeeting < ActiveRecord::Migration[5.2]
  def change
    add_index :meetings, [:training_id, :event_at], unique: true
  end
end
