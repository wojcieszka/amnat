class AddGroupIdToTrainingDay < ActiveRecord::Migration
  def change
    add_column :training_days, :group_id, :integer
  end
end
