class CreatePageContents < ActiveRecord::Migration
  def change
    create_table :page_contents do |t|
      t.integer :page_id
      t.string :title
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
