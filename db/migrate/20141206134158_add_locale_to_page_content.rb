class AddLocaleToPageContent < ActiveRecord::Migration
  def change
    add_column :page_contents, :locale, :string
  end
end
