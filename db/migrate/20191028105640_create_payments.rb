class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.datetime :due_date
      t.integer :member_id
      t.integer :group_id
      t.integer :amount
      t.text :comment

      t.timestamps
    end
  end
end