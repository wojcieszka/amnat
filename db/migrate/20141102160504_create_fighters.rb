class CreateFighters < ActiveRecord::Migration
  def change
    create_table :fighters do |t|
      t.string :name
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
