class CreateSeos < ActiveRecord::Migration
  def change
    create_table :seos do |t|
      t.string :title
      t.text :description
      t.text :keywords
      t.integer :seoable_id
      t.string :seoable_type

      t.timestamps
    end
  end
end
