class AddMemberFields < ActiveRecord::Migration
  def change
    add_column :members, :payment_status, :integer
    add_column :members, :group_id, :integer
    add_column :members, :email, :string
    add_column :members, :date_of_birth, :date
    add_column :members, :phone, :string
  end
end
