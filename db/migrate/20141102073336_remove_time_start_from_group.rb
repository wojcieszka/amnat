class RemoveTimeStartFromGroup < ActiveRecord::Migration
  def change
    remove_column :groups, :time_start, :time
  end
end
