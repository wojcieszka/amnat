class RemoveTimeEndFromGroup < ActiveRecord::Migration
  def change
    remove_column :groups, :time_end, :time
  end
end
