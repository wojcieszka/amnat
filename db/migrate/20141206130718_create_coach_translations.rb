class CreateCoachTranslations < ActiveRecord::Migration
  def up
    Coach.create_translation_table! :description => :text
  end
  
  def down
    Coach.drop_translation_table!
  end
end