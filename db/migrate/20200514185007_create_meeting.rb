class CreateMeeting < ActiveRecord::Migration[5.2]
  def change
    create_table :meetings do |t|
      t.integer :training_id
      t.datetime :event_at
    end
  end
end
