class CreatePageTranslations < ActiveRecord::Migration
  def up
    Page.create_translation_table! :title => :string, :menu_title => :string, :slug => :string
  end
  
  def down
    Page.drop_translation_table!
  end
end