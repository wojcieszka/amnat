class AddLocaleToSeo < ActiveRecord::Migration
  def change
    add_column :seos, :locale, :string
  end
end
