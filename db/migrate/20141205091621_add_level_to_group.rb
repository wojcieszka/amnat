class AddLevelToGroup < ActiveRecord::Migration
  def change
    add_column :groups, :level, :integer
  end
end
