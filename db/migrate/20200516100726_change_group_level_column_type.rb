class ChangeGroupLevelColumnType < ActiveRecord::Migration[5.2]
  def change
    remove_column :groups, :level, :integer
    add_column :groups, :level, :string
  end
end
