class AddImageToNewsPost < ActiveRecord::Migration
  def change
    add_column :news_posts, :image, :string
  end
end
