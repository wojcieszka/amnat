class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.string :name
      t.integer :position
      t.string :menu
      t.string :menu_title

      t.timestamps
    end
  end
end
