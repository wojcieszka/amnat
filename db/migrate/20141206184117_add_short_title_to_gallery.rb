class AddShortTitleToGallery < ActiveRecord::Migration
  def change
    add_column :galleries, :short_title, :string
  end
end
