class AddPersonalTrainingPriceToCoach < ActiveRecord::Migration
  def change
    add_column :coaches, :personal_training_price, :float
  end
end
