class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.time :time_start
      t.time :time_end
      t.string :name
      t.integer :number
      t.string :level

      t.timestamps
    end
  end
end
