class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :token
      t.string :first_name
      t.string :last_name
      t.text :description
      t.integer :status
      t.boolean :declaration

      t.timestamps
    end
  end
end
