class CreatePassTranslations < ActiveRecord::Migration
  def up
    Pass.create_translation_table! :description => :text, :name => :string
  end
  
  def down
    Pass.drop_translation_table!
  end
end
