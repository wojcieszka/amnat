class EventImageUploader < BaseUploader
  
  version :default do
    process resize_to_fit: [240, 1000]
  end
  
end