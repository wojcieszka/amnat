class PartnerImageUploader < BaseUploader

  version :default do
    process resize_to_fit: [300, 100]
  end

end
