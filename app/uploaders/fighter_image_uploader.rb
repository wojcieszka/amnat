class FighterImageUploader < BaseUploader
  
  version :thumb do
    process resize_to_fit: [267, 1000]
  end
  
end