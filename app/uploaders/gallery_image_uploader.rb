class GalleryImageUploader < BaseUploader
  
  version :thumb do
    process resize_to_fill: [160, 160]
  end
  
  version :medium do
    process resize_to_fit: [267, 1000]
  end
  
  version :big do
    process resize_to_fit: [800, 500]
  end
  
end