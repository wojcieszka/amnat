class SlideImageUploader < BaseUploader
  
  version :default do
    process resize_to_fill: [1500, 500]
  end
  
end