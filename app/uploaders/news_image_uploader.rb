class NewsImageUploader < BaseUploader
  
  version :default do
    process resize_to_fit: [543, 1000]
  end
  
end