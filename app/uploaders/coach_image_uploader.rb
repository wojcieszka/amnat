class CoachImageUploader < BaseUploader
  
  version :default do
    process resize_to_fill: [200, 200]
  end
  
end