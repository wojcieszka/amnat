require 'uri'

class ApplicationController < ActionController::Base
  
  include Authorization
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  protect_from_forgery with: :exception
  
  before_action :set_locale
  before_action :load_pages
  helper_method :friendly_page_path
  helper_method :localized_path
  
  
  def self.page(page)
    if page.is_a?(String)
      before_action do
        @page = Page.where(name: page).first!
        @seo = @page.seo
      end
    end
  end
  
  def localized_path(locale)
    url_for({ locale: locale.to_s })
  end
  
  private
  
  def load_pages
    @pages = Page.ordered
  end
  
  def friendly_page_path(page, options = {})
    friendly_page_url(page, options.merge(only_path: true))
  end
  
  def friendly_page_url(page, options = {})
    if Page::SPECIAL_PAGE_NAMES.include?(page.name)
      send("#{page.name}_url", options)
    else
      page_path(page.slug, options)
    end
  end
  
  def set_locale
    I18n.locale = if I18n.available_locales.include?(params[:locale].try(:to_sym))
      params[:locale].to_sym
    else
      I18n.default_locale
    end
  end
  
  def store_location
    unless devise_controller?
      session[:previous_url] = request.fullpath
    end
  end

  protected

  def configure_permitted_parameters
    # Permit the `subscribe_newsletter` parameter along with the other
    # sign up parameters.
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
  end
  
end

