class GalleriesController < ApplicationController
  
  before_action :set_gallery, only: [:edit, :show, :update, :destroy, :add_photo]
  layout false, only: [:show]
  
  page "galleries"
  
  def index
    @galleries = Gallery.ordered
    @galleries = GalleryDecorator.decorate_collection(@galleries)
  end
  
  def new
    @gallery = Gallery.new
    authorize(@gallery)
  end
  
  def create
    @gallery = Gallery.new(gallery_params)
    authorize(@gallery)
    
    if @gallery.save
      redirect_to friendly_page_path(@page), notice: "Album został dodany"
    else
      render action: "new"
    end
  end
  
  def edit
  end
  
  def show
  end
  
  def update
    
    respond_to do |format|
      if @gallery.update_attributes(gallery_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Album został zaktualizowany." }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @gallery.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_path(@page), notice: "Album został usunięty" }
      format.json { head :ok }
    end
  end
  
  def order
    params[:gallery].each_with_index do |id, index|
      Gallery.where(id: id).update_all(position: index)
    end
    flash.now[:notice] = "Kolejność albumów została zapisana."
    render nothing: true
  end
  
  private
  
  def set_gallery
    @gallery = Gallery.find(params[:id])
    authorize(@gallery)
  end
  
  def gallery_params
    params.require(:gallery).permit(:title, :short_title, images_attributes: [:image, :id, :_destroy])
  end
  
end