class GroupsController < ApplicationController
  
  before_action :set_group, only: [:edit, :update, :destroy]
  page "groups"
  def index
    @groups = Group.ordered
    @groups = GroupDecorator.decorate_collection(@groups)
  end
  
  def new
    @group = Group.new
    authorize(@group)
  end
  
  def create
    @group = Group.new(group_params)
    authorize(@group)
    respond_to do |format|
      if @group.save
        format.html { redirect_to groups_path, notice: "Grupa została dodana" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def show
  end
  
  def update
    respond_to do |format|
      if @group.update_attributes(group_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Grupa została zaktualizowana." }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @group.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_path(@page), notice: "Grupa została usunięta" }
      format.json { head :ok }
    end
  end
  
  def order
    params[:group].each_with_index do |id, index|
      Group.where(id: id).update_all(position: index)
    end
    flash.now[:notice] = "Kolejność grup została zapisana."
    render nothing: true
  end
  
  private
  
  def set_group
    @group = Group.find(params[:id])
    authorize(@group)
  end
  
  def group_params
    params.require(:group).permit(:name, :number, :level, trainings_attributes: [:group_id, :name, :id, :_destroy, :time_start, :time_end])
  end
  
end