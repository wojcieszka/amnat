class PartnersController < ApplicationController
  
  page "partners"
  
  def new
    @partner = Partner.new
    authorize(@partner)
  end
  
  def create
    @partner = Partner.create(partner_params)
    authorize(@partner)
    respond_to do |format|
      if @partner.save
        format.html { redirect_to passes_path, notice: "Partner został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def destroy
    @partner = Partner.find(params[:id])
    authorize(@partner)
    @partner.destroy
    
    respond_to do |format|
      format.html { redirect_to passes_url, notice: "Partner został usunięty" }
      format.json { head :ok }
    end
  end  
  
  private
  
  def partner_params
    params.require(:partner).permit(:name, :link, :image)
  end
  
  
  
end