class RegistrationsController < Devise::RegistrationsController

  protected

  def after_sign_up_path_for(resource)
    new_session_path
    #after_sign_in_path_for(resource) if is_navigational_format?
  end


end