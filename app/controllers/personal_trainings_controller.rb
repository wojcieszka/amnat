class PersonalTrainingsController < ApplicationController

  page "personal_trainings"

  def index
    @coaches = CoachesDecorator.decorate_collection(Coach.all)
  end
end