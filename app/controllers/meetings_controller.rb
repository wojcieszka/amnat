class MeetingsController < ApplicationController
  page "meetings"

  def index
    authorize(Meeting, :index?)
    @meetings = Meeting.active.all.decorate
  end

  def new
    if meeting_service.create_week_meetings
      redirect_to trainings_path, notice: "Zajęcia zostały dodane."
    else
      redirect_to trainings_path, alert: "Wystąpiły błędy. Nie udało się otworzyć zajęć na przyszły tydzień"
    end
  end

  def sign_up
    if meeting_service.sign_up(current_user)
      redirect_to trainings_path, notice: "Zapisałeś się na zajęcia"
    else
      redirect_to trainings_path, alert: meeting_service.meeting.errors
    end
  end

  def cancel
    meeting_service.cancel_user_meeting(current_user)
    redirect_to user_meetings_path(current_user), notice: "Wypisałeś się z zajęć"
  end

  private

  def meeting_service
    @service ||= MeetingService.new(meeting_id: params[:id])
  end

end