class FaqsController < ApplicationController
  
  before_action :set_faq, only: [:edit, :update, :destroy]
  page "faqs"
  
  def index
    @faqs = Faq.ordered
  end
  
  def new
    @faq = Faq.new
    authorize(@faq)
  end
  
  def create
    @faq = Faq.new(faq_params)
    authorize(@faq)

    respond_to do |format|
      if @faq.save
        format.html { redirect_to friendly_page_url(@page), notice: "Pytanie zostało dodane" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @faq.update_attributes(faq_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Pytanie zostało zaktualizowane" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @faq.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Pytanie zostało usunięte" }
      format.json { head :ok }
    end
  end
  
  private
  
  def set_faq
    @faq = Faq.find(params[:id])
    authorize(@faq)
  end
  
  def faq_params
    params.require(:faq).permit(:question, :answer)
  end
  
  
end