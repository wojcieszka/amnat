class CommentsController < ApplicationController
  
  def new
    @comment = Comment.new
  end
  
  def create
    @comment = Comment.new(comment_params)
    
    respond_to do |format|
      if @comment.save
        format.html { redirect_to news_posts_path, notice: "Twój komentarz został dodany" }
      else
        format.html { redirect_to news_posts_path, alert: "Twój komentarz nie został dodany" }
      end
    end
    
  end
  
  def destroy
    @comment = Comment.find(params[:id])
    authorize(@comment)
    @comment.destroy
    
    respond_to do |format|
      format.html { redirect_to news_posts_path, notice: "Komentarz został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def comment_params
    params.require(:comment).permit(:content, :author, :news_post_id, :humanizer_answer, :humanizer_question_id)
  end
  
end