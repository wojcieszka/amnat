class FightersController < ApplicationController
  
  before_action :set_fighter, only: [:edit, :update, :destroy]
  page "fighters"
  
  def index
    @fighters = Fighter.all
  end
  
  def show
  end
  
  def new
    @fighter = Fighter.new
    authorize(@fighter)
  end
  
  def create
    @fighter = Fighter.new(fighter_params)
    authorize(@fighter)
    respond_to do |format|
      if @fighter.save
        format.html { redirect_to fighters_path, notice: "Zawodnik został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @fighter.update_attributes(fighter_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Album został zaktualizowany." }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @fighter.destroy
    
    respond_to do |format|
      format.html { redirect_to fighters_path, notice: "Zawodnik został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def set_fighter
    @fighter = Fighter.find(params[:id])
    authorize(@fighter)
  end
  
  def fighter_params
    params.require(:fighter).permit(:name, :description, :image)
  end
  
end