class PassesController < ApplicationController
  
  before_action :set_pass, only: [:edit, :update, :destroy]
  page "passes"
  
  def index
    @passes = Pass.all
    @partners = Partner.all
  end
  
  def show
  end
  
  def new
    @pass = Pass.new
    authorize(@pass)
  end
  
  def create
    @pass = Pass.create(pass_params)
    authorize(@pass)
    respond_to do |format|
      if @pass.save
        format.html { redirect_to passes_path, notice: "Karnet został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @pass.update_attributes(pass_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Karnet został zaktualizowany" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @pass.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Karnet został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def set_pass
    @pass = Pass.find(params[:id])
    authorize(@pass)
  end
  
  def pass_params
    params.require(:pass).permit(:name, :description, :price)
  end
  
  
  
end