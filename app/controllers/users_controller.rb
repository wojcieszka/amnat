class UsersController < ApplicationController
  
  before_action :set_user, only: [:edit, :show, :update, :destroy]
  
  page "users"
  
  def index
    @users = User.all
    authorize(@users)
  end
  
  def new
    @user = User.new
    authorize(@user)
  end
  
  def create
    @user = User.new(user_params)
    authorize(@user)

    if @user.save
      redirect_to users_path, notice: 'Administrator został dodany'
    else
      render action: 'new'
    end
    
  end
  
  def show
  end
  
  def edit
  end
  
  def update
    
    respond_to do |format|
      if @user.update(user_params)
        sign_in(@user, :bypass => true)
        format.html { redirect_to root_path, notice: 'Twoje dane zostały zaktualizowane' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_user
    @user = User.find(params[:id])
    authorize(@user)
  end
  
  def user_params
    params.require(:user).permit(:email, :password, :first_name, :last_name)
  end
  
end