class PagesController < ApplicationController
  
  before_action :set_page, only: [:edit, :destroy, :update]
  
  def index
  end
  
  def new
    @page = Page.new
    authorize(@page)
    
    respond_to do |format|
      format.html
      format.json { render json: @page }
    end
  end
  
  def create
    @page = Page.new(page_params)
    authorize(@page)
    respond_to do |format|
      if @page.save
        format.html { redirect_to  friendly_page_url(@page), notice: "Strona została utworzona." }
        format.json { render json:  @page, status: :created, location: @page }
      else
        format.html { render action: "new" }
        format.json { render json:  @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
    @page = Page.find_by(slug: params[:id])
    @page = PageDecorator.decorate(@page)
    
    respond_to do |format|
      format.html
    end
  end
  
  def edit
  end
  
  def update
    
    respond_to do |format|
      if @page.update_attributes(page_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Strona została zapisana." }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @page.destroy

    respond_to do |format|
      format.html { redirect_to root_url, notice: "Strona została usunięta." }
      format.json { head :ok }
    end
  end
  
  def order
    params[:page].each_with_index do |id, index|
      Page.where(id: id).update_all(position: index)
    end
    flash.now[:notice] = "Kolejność stron została zapisana."
    render nothing: true
  end
  
  private
  
  def set_page
    @page = Page.find(params[:id])
    authorize(@page)
  end
  
  def page_params
    params.require(:page).permit(:title, :slug, :menu_title, :menu, :position, :parent_id, contents_attributes: [:id, :content],
                                 seo_attributes: [:id, :title, :description, :keywords])
  end
  
end