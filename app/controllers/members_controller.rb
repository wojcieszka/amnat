class MembersController < ApplicationController
  before_action :load_member, only: [:destroy, :show]

  layout "bootstrap"

  def index
    @members = Member.ordered
    @members = MemberDecorator.decorate_collection(@members)
    authorize(@members)
  end

  def new
    @member = Member.new
    authorize(@member)
  end

  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to members_path, notice: "Członek został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end

  def show
    @member = @member.decorate
  end

  def edit
    @member = Member.find(params[:id])
  end

  def update
    @member = Member.find(params[:id])
    respond_to do |format|
      if @member.update_attributes(member_params)
        format.html { redirect_to members_path, notice: "Członek został dodany" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end

  private

  def member_params
    params.require(:member).permit(
      :first_name, :last_name, :description, :status, :token, :date_of_birth,
      :email, :phone
      )
  end

  def load_member
    @member = Member.find_by_id_and_token(params[:id], params[:token])
    authorize(@member)
  end



end
