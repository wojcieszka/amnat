class CoachesController < ApplicationController
  
  before_action :set_coach, only: [:edit, :update, :destroy]
  page "coaches"
  
  def index
    @coaches = Coach.all
  end
  
  def new
    @coach = Coach.new
    authorize(@coach)
  end
  
  def create
    @coach = Coach.new(coach_params)
    authorize(@coach)
    
    respond_to do |format|
      if @coach.save
        format.html { redirect_to coaches_path, notice: "Instruktor został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @coach.update_attributes(coach_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Instruktor został zaktualizowany" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @coach.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Instruktor został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def set_coach
    @coach = Coach.find(params[:id])
    authorize(@coach)
  end
  
  def coach_params
    params.require(:coach).permit(:name, :description, :image, :personal_training_price)
  end
end