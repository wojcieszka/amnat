class EventsController < ApplicationController
  
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  page "events"
  
  def index
    @events = Event.all
  end
  
  def show
    @event = EventDecorator.decorate(@event)
  end
  
  def new
    @event = Event.new
    authorize(@event)
  end
  
  def create
    @event = Event.new(event_params)
    authorize(@event)
    respond_to do |format|
      if @event.save
        format.html { redirect_to friendly_page_url(@page), notice: "Wydarzenie zostało dodane" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
    authorize(@event)
  end
  
  def update
    authorize(@event)
    respond_to do |format|
      if @event.update_attributes(event_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Wydarzenie zostało zaktualizowane" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    authorize(@event)
    @event.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Wydarzenie zostało usunięte" }
      format.json { head :ok }
    end
  end
  
  private
  
  def event_params
    params.require(:event).permit(:title, :description, :starts_at, :venue, :city, :image)
  end
  
  def set_event
    @event = Event.friendly.find params[:id]
  end
  
end