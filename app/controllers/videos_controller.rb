class VideosController < ApplicationController
  
  page "videos"
  
  def index
    @videos = Video.all
  end
  
  def new
    @video = Video.new
    authorize(@video)
  end
  
  def create
    @video = Video.create(video_params)
    authorize(@video)
    respond_to do |format|
      if @video.save
        format.html { redirect_to friendly_page_url(@page), notice: "Film został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def destroy
    @video = Video.find(params[:id])
    authorize(@video)
    
    @video.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Film został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def video_params
    params.require(:video).permit(:link)
  end
  
end