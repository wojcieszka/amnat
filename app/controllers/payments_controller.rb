class PaymentsController < ApplicationController

  def index
  end

  def new
  end

  def create
    @member = Member.find(params[:member_id])
    @payment = @member.payments.build(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to member_path(@member, token: @member.token), notice: "Płatność została dodana" }
      else
        format.html { redirect_to member_path(@member, token: @member.token), alert: "Płatność nie została dodana" }
      end
    end
  end

  private

  def payment_params
    params.require(:payment).permit(:amount, :comment, :group_id)
  end


end