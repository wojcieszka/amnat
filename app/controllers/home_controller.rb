class HomeController < ApplicationController
  
  page "root"

  def index
    @slides = Slide.all
    @events_page = Page.find_by_name("events")
    @events_page = HomeDecorator.decorate(@events_page)
    @muaythai_page = Page.find_by_name("muaythai")
    @muaythai_page = HomeDecorator.decorate(@muaythai_page)
    @club_page = Page.find_by_name("about_us")
    @club_page = HomeDecorator.decorate(@club_page)
  end
 
end 