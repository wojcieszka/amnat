class SlidesController < ApplicationController
  
  before_action :set_slide, only: [:edit, :update, :destroy]
  
  def new
    @slide = Slide.new
    authorize(@slide)
  end
  
  def create
    @slide = Slide.create(slide_params)
    authorize(@slide)
    respond_to do |format|
      if @slide.save
        format.html { redirect_to root_path, notice: "Slajd został dodany" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @slide.update_attributes(slide_params)
        format.html { redirect_to root_path, notice: "Slajd został uaktualniony" }
      else
        format.html { render action: "edit" }
      end
    end
  end
  
  def destroy
    @slide.destroy
    
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Slajd został usunięty" }
      format.json { head :ok }
    end
  end
  
  private
  
  def set_slide
    @slide = Slide.find(params[:id])
    authorize(@slide)
  end
  
  def slide_params
    params.require(:slide).permit(:image, :position)
  end
  
end