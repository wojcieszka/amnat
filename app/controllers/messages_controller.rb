class MessagesController < ApplicationController
  
  before_action :set_message, only: [ :read, :show ]
  
  page "contact"
  
  def index
    @messages = Message.ordered
    authorize(@messages)
    @messages = PaginatingDecorator.decorate(@messages.page(params[:page]).per(20), with: MessagesDecorator)
    
  end
  
  def new
    @message = Message.new
  end
  
  def create
    @message = Message.new(message_params)
    
    respond_to do |format|
      if @message.save
        send_email_to_admin(@message)
        format.html { redirect_to friendly_page_url(@page), notice: "Twoja wiadomość została wysłana" }
        format.json { render json: @page, status: :created, location: @page }
      else
        format.html { redirect_to friendly_page_url(@page), alert: "Twoja wiadomość nie została wysłana" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
    @message = MessagesDecorator.decorate(@message)
  end
  
  def read
    @message.status = "read"
    respond_to do |format|
      if @message.save
        format.html { redirect_to messages_path, notice: "Wiadomość została oznaczona jako przeczytana"}
      else
        format.html { redirect_to messages_path, alert: "Nie udało się oznaczyć wiadomości"}
      end
    end
  end
  
  private
  
  def set_message
    @message = Message.find(params[:id])
    authorize(@message)
  end
  
  def message_params
    params.require(:message).permit(:author, :content, :title, :phone, :email, :humanizer_answer, :humanizer_question_id, :status)
  end
  
  def send_email_to_admin(message)
    MessageMailer.send_email(message).deliver
  end
  
end