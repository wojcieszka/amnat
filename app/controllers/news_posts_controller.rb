class NewsPostsController < ApplicationController
  
  before_action :set_news_post, only: [:edit, :update, :destroy, :show]
  page "news_posts"
  
  def index
    @news_posts = PaginatingDecorator.decorate(NewsPost.ordered.page(params[:page]).per(10), with: NewsPostsDecorator)
    @comment = Comment.new
    @events = Event.future.ordered.limit(4).decorate
  end
  
  def show
  end
  
  def new
    @news_post = NewsPost.new
  end
  
  def edit
  end
  
  def create
    @news_post = NewsPost.create(news_post_params)
    respond_to do |format|
      if @news_post.save
        format.html { redirect_to news_posts_path, notice: "Aktualność została dodana" }
      else
        format.html { render action: "new" }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @news_post.update_attributes(news_post_params)
        format.html { redirect_to friendly_page_url(@page), notice: "Aktualność zostało zaktualizowana" }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @news_post.destroy
    
    respond_to do |format|
      format.html { redirect_to friendly_page_url(@page), notice: "Aktualność została usunięta" }
      format.json { head :ok }
    end
  end
  
  private
  
  def news_post_params
    params.require(:news_post).permit(:title, :content, :image)
  end
  
  def set_news_post
    @news_post = NewsPost.find(params[:id])
  end
  
end