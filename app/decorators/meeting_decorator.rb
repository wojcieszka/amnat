class MeetingDecorator < Draper::Decorator
  delegate_all

  def users_count(opts = {})
    with_number = opts[:with_number]
    h.content_tag :span, users_count_bar(object.users.count, with_number), class: "meeting-users-count"
  end

  def with_user?(user)
    return false if user.nil?
    user.id.in?(object.users.ids)
  end

  def sign_up_message(user)
    position = object.users.sign_up_list.index(user) + 1
    if position > max_users_count
      h.content_tag :div, h.t("meeting.you_are_on_reserve_list"), class: "reserve-list-info"
    else
      h.content_tag :div, h.t("meeting.you_are_registered"), class: "registration-info"
    end
  end

  def event_at_formatted
    object.event_at.utc.strftime("%d.%m.%Y - %H:%M")
  end

  def event_at_date
    object.event_at.utc.strftime("%d.%m.%Y")
  end

  def event_at_time
    object.event_at.utc.strftime("%H:%M")
  end

  def max_users_count
    Meeting::MAX_USERS_COUNT
  end

  private

  def users_count_bar(count, with_number)
    width = count.to_f / max_users_count * 100
    width = 100 if width > 100
    bar = h.content_tag(:span, "", class: "users-count-bar", style: "width: #{width}%")
    return bar unless with_number
    ["#{count} / #{max_users_count}", bar].join.html_safe
  end

end