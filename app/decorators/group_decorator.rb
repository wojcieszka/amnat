class GroupDecorator < Draper::Decorator
  delegate_all
  
  def training_day(day)
    trainings(day).present? ? time_text(day).html_safe : ""
  end
  
  def sortable_id
    h.can?(:update, Group) && "group_#{model.id}"
  end

  def get_meeting(day)
    meeting = object.meetings.active.detect do |meeting|
      date = meeting.event_at
      meeting_day = date.strftime("%A").downcase
      meeting_day == day && date.future?
    end
  end
  
  private

  def time_text(day)
    start_hour(day) + " " + start_minute(day) + " - " + end_hour(day) + end_minute(day)
  end
  
  def start_hour(day)
    h.content_tag :span, current_day(day).time_start.strftime("%H"), class: "hour"
  end
  
  def start_minute(day)
    h.content_tag :span, current_day(day).time_start.strftime("%M"), class: "minute"
  end
  
  def end_hour(day)
    h.content_tag :span, current_day(day).time_end.strftime("%H"), class: "hour"
  end
  
  def end_minute(day)
    h.content_tag :span, current_day(day).time_end.strftime("%M"), class: "minute"
  end
  
  def current_day(day)
    trainings(day).first
  end

  def trainings(day)
    object.trainings.where(name: day)
  end
  
end