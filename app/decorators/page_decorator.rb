class PageDecorator < Draper::Decorator
  delegate :title, :id, :slug, :name, :parent, :children, :menu_title
  
  def content
    model.content.try(:html_safe)
  end
  
  def active?
    model == current_page || (current_page.present? && current_page.parent == model)
  end
  
  def sortable_id
    h.can?(:update, Page) && "page_#{model.id}"
  end
  
  def active_class
    PageDecorator.decorate(model, context: context).active? && "current"
  end
  
  def has_subpages?
    model.children.any?
  end
  
  private
  
  def current_page
    context[:current_page]
  end

end
