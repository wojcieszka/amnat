class EventDecorator < Draper::Decorator
  
  def date
    h.safe_join([day, month])
  end
  
  def date_with_details
    h.safe_join([day, month, day_of_week, time])
  end
  
  def title
    h.content_tag :span, object.title, class: "title"
  end
  
  def city
    h.content_tag :span, object.city, class: "city"
  end
  
  def venue
    h.content_tag :span, object.venue, class: "venue"
  end
  
  def short_title
    h.content_tag :span, (h.truncate(object.title, length: 32)), class: "title"
  end
  
  def description
    h.content_tag :div, object.description.html_safe, class: "description"
  end
  
  def image
    h.image_tag object.image.default.url, class: "image"
  end
  
  private
  
  def day
    h.content_tag :span, (l object.starts_at, format: ("%d")), class: "day"
  end
  
  def month
    h.content_tag :span, (l object.starts_at, format: ("%B")), class: "month"
  end
  
  def time
    h.content_tag :span, (l object.starts_at, format: ("%H:%M")), class: "time"
  end
  
  def day_of_week
    h.content_tag :span, (l object.starts_at, format: ("%a")), class: "day-of-week"
  end
  
  

  
end