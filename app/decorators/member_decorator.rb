class MemberDecorator < Draper::Decorator
  delegate_all

  def group_name
    return if group.nil?
    group.name
  end

  def ordered_payments
    object.payments
    # PaymentsDecorator.decorate_collection(object.payments)
  end

end