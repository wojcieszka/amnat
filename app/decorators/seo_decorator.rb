class SeoDecorator < Draper::Decorator
  def meta_tags
    h.safe_join([title, description, author, image, type, url].flatten.reject(&:blank?), "\n")
  end
  
  def title
    og_tag(:title, model.title) if model.title.present?
  end
  
  def description
    if model.description.present?
      [meta_tag("description", model.description), og_tag(:description, model.description)]
    end
  end
  
  def image
    og_tag(:image, url_from_path(og_image_path))
  end
  
  def author
    meta_tag "author", Seo::DEFAULT_AUTHOR
  end
  
  def type
    og_tag("type", model.og_type)
  end
  
  def url
    og_tag("url", h.request.original_url)
  end
  
  private
  
  def url_from_path(path)
    URI.join(h.root_url, path).to_s
  end
  
  def og_image_path
    if model.image.present?
      model.image
    else
      h.asset_path("og-logo.png")
    end
  end
  
  def og_tag(property, content)
    h.tag(:meta, property: "og:#{property}", content: h.strip_tags(content))
  end
  
  def meta_tag(property, content)
    h.tag(:meta, name: property, content: h.strip_tags(content))
  end
end
