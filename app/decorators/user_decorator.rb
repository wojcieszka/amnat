class UserDecorator < Draper::Decorator

  delegate_all

  def full_name
    [object.first_name, object.last_name].join(" ")
  end

  def subscribe_date(meeting)
    date = object.user_meetings.where(meeting_id: meeting.id, user_id: object.id).first.created_at
    date.strftime("%d.%m.%Y - %H:%M")
  end

end