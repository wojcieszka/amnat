class HomeDecorator < Draper::Decorator
  delegate_all
  
  def content
    h.truncate(text, length: 280)
  end
  
  private
  
  def text
    h.strip_tags(object.content)
  end

  
end