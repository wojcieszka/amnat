class CoachesDecorator < Draper::Decorator

  delegate_all

  def standard_package_price
    return if personal_training_price.nil?
    h.personal_training_price(personal_training_price)
  end

  def small_package_price
    return if personal_training_price.nil?
    h.personal_training_price(personal_training_price - 10)
  end

  def premium_package_price
    return if personal_training_price.nil?
    h.personal_training_price(personal_training_price - 20)
  end

end