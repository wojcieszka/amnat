class GalleryDecorator < Draper::Decorator
  delegate_all
  
  def sortable_id
    h.can?(:update, Gallery) && "gallery_#{model.id}"
  end
  
end