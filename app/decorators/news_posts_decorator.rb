class NewsPostsDecorator < Draper::Decorator
  delegate_all
  
  def day
    l object.created_at, format: ("%d")
  end
  
  def month
    l object.created_at, format: ("%B")
  end
  
  def content
    object.content.try(:html_safe)
  end
  
  def image
    if object.image.present?
      h.content_tag :div, class: "news-post-image" do
        h.image_tag object.image.default.url
      end
    end
  end
  
end