class MessagesDecorator < Draper::Decorator
  
  delegate :title, :content, :author, :phone, :email
  
  def created_at
    h.short_date(object.created_at)
  end
  
  def short_content
    h.truncate(object.content, length: 30)
  end
  
  def status
    h.content_tag :span, I18n.t("message.statuses.#{object.status}"), class: "message-status #{object.status}"
  end
  
  def fresh?
    object.status == "fresh"
  end
  
end