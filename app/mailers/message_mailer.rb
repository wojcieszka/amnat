class MessageMailer < ActionMailer::Base
  include Roadie::Rails::Automatic
  
  default from: "Am Nat <klub@amnat.pl>", charset: "UTF-8"
  
  def send_email(message)
    @message = message
    mail(to: "klub@amnat.pl", subject: "Dostałeś nową wiadomość")
  end
  
end