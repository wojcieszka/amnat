class FighterDescription
  
  constructor: (el) ->
    @$el = $(el)
    
    @setTop()
    
    
  setTop: =>
    top = @$el.height() * (-1) - 40
    @$el.css("top", top + "px")

ready = ->
    
    $("[data-role=fighter-description]").each (i, el) ->
      new FighterDescription(el)

$(document).on('turbolinks:load', ready)