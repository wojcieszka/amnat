wysihtml5ParserRules =
  tags:
    strong: {}
    b:      {}
    i:      {}
    em:     {}
    br:     {}
    p:      {}
    div:    {}
    span:   {}
    ul:     {}
    ol:     {}
    li:     {}
    a:
      check_attributes:
        href:   "url"


ready = ->

  $("[data-role~='wysihtml5-input']").each (index, el) =>
    input_id = $("textarea", el).attr("id")
    toolbar_id = $("[data-role=toolbar]", el).attr("id")
    new wysihtml5.Editor(input_id, { toolbar: toolbar_id, parserRules: wysihtml5ParserRules, stylesheets: ["/assets/application.css"] })

$(document).on('turbolinks:load', ready)
