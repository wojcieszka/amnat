class GalleryCarousel
  currentIndex: 0
  currentNavIndex: 0
  itemsCount: 0
    
  constructor: (el) ->
    @$el = $(el)
    @$list = @$el.find("[data-role=gallery-list]")
    @$slideItem = @$el.find("[data-role=gallery-item]")
    @$itemsCount = @$el.find("[data-role=gallery-item]").length
    @$navItem = @$el.find("[data-role=gallery-nav-item]")
    @$navPrev = @$el.find("[data-role=gallery-nav-prev]")
    @$navNext = @$el.find("[data-role=gallery-nav-next]")
    @$wrapper = $(".slideshow-slides-container-wrapper")
    
    @$prev = @$el.find("[data-role=gallery-prev]")
    @$next = @$el.find("[data-role=gallery-next]")
    
    @$galleryNav = @$el.find("[data-role=gallery-nav]")
    @$galleryNavCount =  Math.ceil(@$itemsCount / 5)

    @setGalleryNavWidth()
    @$navItem.on "click", @setCurrentIndex
    @$navNext.on "click", @showNextNavItems
    @$navPrev.on "click", @showPrevNavItems
    @$next.on "click", @goToNext
    @$prev.on "click", @goToPrev
    @goToCurrent()
    @goToGalleryNav()
    $(window).on 'keydown', @handleKeydown
    $(window).on "resize", @handleResize
  
  setGalleryNavWidth: =>
    width = @$itemsCount * 160
    @$galleryNav.css("width", width + "px")      
    
  setCurrentIndex: (e) =>
    @currentIndex = $(e.currentTarget).index()
    @goToCurrent()    
    
  showNextNavItems: =>
    @currentNavIndex++
    @goToGalleryNav()      
    
  showPrevNavItems: =>
    @currentNavIndex--
    @goToGalleryNav()
    
  goToGalleryNav: =>
    @$navPrev.removeClass "hidden"
    @$navNext.removeClass "hidden"
    left = @currentNavIndex * (-800) 
    @$galleryNav.css("left", left + "px")
    if @currentNavIndex == @$galleryNavCount-1
      @$navNext.addClass "hidden"
    if @currentNavIndex == 0
      @$navPrev.addClass "hidden"
    
  checkGalleryNavPosition: =>
    i = Math.ceil(@$itemsCount / 5)
    array = [0..i]
    min = 0
    max = 4
    currentIndex = @currentIndex
    currentNavIndex = @currentNavIndex
    for a in array
      do ->
        if currentIndex in [min..max]
          currentNavIndex = a
      min = min+5
      max = max+5
      
    @currentNavIndex = currentNavIndex
    @goToGalleryNav()
    
  goToNext: =>
    @currentIndex++
    @goToCurrent()
    
  goToPrev: =>
    @currentIndex--
    @goToCurrent()
    
  goToCurrent: =>
    @$prev.removeClass "hidden"
    @$next.removeClass "hidden"
    @$navItem.removeClass "current"
    @$slideItem.removeClass "current"
    @$navItem.eq(@currentIndex).addClass "current"
    @$slideItem.eq(@currentIndex).addClass "current"
    if @currentIndex == @$itemsCount-1
      @$next.addClass "hidden"
    if @currentIndex == 0
      @$prev.addClass "hidden"
    @checkGalleryNavPosition()
    
  handleKeydown: (e) =>
    lastItem = (@currentIndex == @$itemsCount-1)
    firstItem = (@currentIndex == 0)
    if (e.keyCode == 39 || e.keyCode == 32 || e.keyCode == 13) and lastItem isnt true
      @goToNext()
    else if e.keyCode == 37 and firstItem isnt true
      @goToPrev()
    else if e.keyCode == 27 
      @close()
    else
      e.preventDefault()
      
  handleResize: () =>
    height = $(window).height() - (120 + 60 + 232)
    @$wrapper.css("height", height)
    
  close: =>
    $("[data-role=slideshow]").removeClass "visible"
    $(window).off 'keydown', @handleKeydown
    $(window).off 'resize', @handleResize
    
class Slideshow
  currentIndex: 0
  imagesCount: 0
    
  constructor: (el, galleryLink) ->
    @$el = $(el)
    @$galleryLink = galleryLink
    @$container = $("[data-role=slides-container]")
    @$wrapper = @$el.find(".slideshow-slides-container-wrapper")
    @$closeButton = @$el.find("[data-action=close-slideshow]")
    @$closeButton.on "click", @closePopover
    @$el.on "click", @closePopover
    
    @$wrapper.on "click", (e) ->
      e.stopPropagation()
      e.preventDefault()
      
    @showPopover()
    
      
  showPopover: =>
    @$el.addClass "visible"
    
    $.ajax
      url: @$galleryLink
      cache: true
      success: (html) =>
        @$container.empty()
        @$container.append html
        carousel = @$container.find("[data-role=gallery-carousel]")
        new GalleryCarousel(carousel)
        
        
  closePopover: =>
    @$el.removeClass "visible" 


ready = ->
  $('a[data-role=slideshow-open]').on "click", (e) =>
    e.preventDefault()
    galleryLink = e.currentTarget.href
    
    $("[data-role=slideshow]").each (i, el) ->
      new Slideshow(el, galleryLink)

$(document).on('turbolinks:load', ready)