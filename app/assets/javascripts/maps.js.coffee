ready = ->
  console.log window.location.pathname
  loadMap = ->
    myLatlng = new google.maps.LatLng(50.0661510, 19.9694640)
    myOptions =
      zoom: 16
      center: myLatlng
      disableDefaultUI: true
      panControl: true
      zoomControl: true
      zoomControlOptions:
        style: google.maps.ZoomControlStyle.DEFAULT

      mapTypeControl: true
      mapTypeControlOptions:
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR

      streetViewControl: true
      mapTypeId: google.maps.MapTypeId.ROADMAP

    map = new google.maps.Map(document.getElementById("map"), myOptions)
    marker = new google.maps.Marker(
      position: myLatlng
      map: map
      title: "Kraków, Cystersów 3"
    )
    return
  
  if window.location.pathname.match(/kontakt/) || window.location.pathname.match(/contact/) || window.location.pathname.match(/контакт/)
    loadMap()

$(document).on('turbolinks:load', ready)

