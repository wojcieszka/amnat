ready = ->
  
  $scrollEl = $("[data-role=scroll-top-element]")
  
  $(window).on "scroll", ->
    if $(window).scrollTop() > 500
      $scrollEl.addClass "visible"
    else
      $scrollEl.removeClass "visible"
      
  $scrollEl.on "click", ->
    $("html, body").animate(
      scrollTop: 0,
      300,
      "swing"
      )

$(document).on('turbolinks:load', ready)