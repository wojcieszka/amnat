class CommentsToggle
    
  constructor: (el) ->
    @$el = $(el)
    @$link = @$el.find("[data-role=show-comments-link]")
    @$comments = @$el.find("[data-role=comments]")
    
    @$link.on "click", @handleComments
    @setElementHeight()
    @addHiddenClass()
    
  setElementHeight: =>
    @$el.css("height", @$comments.height())
    
  addHiddenClass: =>
    @$el.addClass "hidden"  
    
  handleComments: (e) =>
    e.preventDefault()
    @$el.toggleClass "hidden"



ready = ->
    
  $("[data-role=comments-wrapper]").each (i, el) ->
    new CommentsToggle(el)
      
$(document).on('turbolinks:load', ready)