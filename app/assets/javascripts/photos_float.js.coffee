ready = ->
  $container = $("[data-masonry=true]")
  
  $container.imagesLoaded ->
    if $container.hasClass "gallery-list"
      $container.masonry(
        columnWidth: 277,
        itemSelector: ".gallery-item"
      )
    else if $container.hasClass "fighters-list"
      $container.masonry(
        columnWidth: 277,
        itemSelector: ".fighter-item"
      )


$(document).on('turbolinks:load', ready)
  