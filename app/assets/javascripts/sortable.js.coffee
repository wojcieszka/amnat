class Sortable
    
  constructor: (el) ->
    @$el = $(el)
    @$sortableList = @$el.find("[data-role=sortable-list]")
    @$toggleSort = @$el.find("[data-action=toggle-sort]")
    
    @$toggleSort.on "click", @handleToggleSortClick
    @masonryElement = @$el.find("[data-masonry=true]")
    
    @url = @$el.data("updateUrl")
    @key = @$el.data("sortableKey")
    @updateSelector = @$el.data("sortableUpdate")
    @enabled = false
    @initialized = false
    @toggable = if @$el.data("toggable")? then @$el.data("toggable") == "true" else true
    
    @enable() unless @toggable
    
  handleToggleSortClick: =>
    event.preventDefault()
    @toggle()
    @masonryElement.find("li").css("position", "static")
    
  toggle: =>
    if @enabled
      @disable()
    else
      @enable()
      
  initSortable: =>
    return if @initialized
    @handleExists = @$el.find("[data-role=handle]").length > 0
    @$el.addClass "sortable-with-handle" if @handleExists
    @$el.sortable 
      items: "[data-role=sortable-list] > .sortable-item"
      update: @handleUpdate
      handle: @handleExists && "[data-role=handle]"
    @initialized = true
      
  enable: =>
    @initSortable()
    @$el.sortable 'enable'
    @enabled = true
  
  disable: =>
    return unless @toggable
    @initSortable()
    @$el.sortable 'disable'
    @enabled = false
    
  masonry: =>
    @masonryElement.masonry('reloadItems')
    
  handleUpdate: =>
    $.ajax
      type: "post"
      url: @url
      data: @$el.sortable("serialize", key: @key)
      complete: @handleComplete
    @masonryElement.imagesLoaded @masonry if @masonryElement.length > 0
      
  handleComplete: (xhr) =>
    @disable()
    if @updateSelector?
      html = $(xhr.responseText).find @updateSelector
      $(@updateSelector).replaceWith html



ready = ->
    
  $("[data-role=sortable]").each (i, el) ->
    new Sortable(el)

$(document).on('turbolinks:load', ready)