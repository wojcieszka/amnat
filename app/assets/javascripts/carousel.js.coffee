class Carousel
  currentIndex: 0
  itemsCount: 0
    
  constructor: (el) ->
    @$el = $(el)
    @$list = @$el.find("[data-role=carousel-list]")
    @$slideItem = @$el.find("[data-role=carousel-item]")
    @$itemsCount = @$el.find("[data-role=carousel-item]").length
    @$nextButton = @$el.find("[data-action=next-carousel-item]")
    @$prevButton = @$el.find("[data-action=previous-carousel-item]")
    @$navItem = @$el.find("[data-role=nav-item]")
    
    @$nextButton.on "click", @goToNext
    @$prevButton.on "click", @goToPrev
    @$navItem.on "click", @setCurrentIndex
    @goToCurrent()
    imagesLoaded(@$el, @setWrapperHeight)
    $(window).on "resize", @setWrapperHeight
    setInterval(@goToNext, 4000)
    
  
  setWrapperHeight: =>
    height = @$slideItem.find("img").first().height()
    if height > 500
      @$el.css("height", "500px")
    else
      @$el.css("height", height)
    
  setCurrentIndex: (e) =>
    @currentIndex = $(e.currentTarget).index()
    @goTo @currentIndex
    
  goToNext: =>
    @goTo @currentIndex + 1
    
  goToPrev: =>
    @goTo @currentIndex - 1
    
  goTo: (index) =>
    index += @$itemsCount while index < 0
    @currentIndex = index % @$itemsCount
    @goToCurrent()
    
  goToCurrent: () =>
    @$slideItem.removeClass "current"
    @$slideItem.removeClass "next"
    @$slideItem.removeClass "left"
    @$navItem.removeClass "current"
    @$navItem.eq(@currentIndex).addClass "current"
    @$slideItem.eq(@currentIndex).addClass "current"
    @$slideItem.eq(@currentIndex+1).addClass "next"
    @$slideItem.eq(@currentIndex-1).addClass "left"
    if @currentIndex == @$itemsCount-1
      @$slideItem.eq(0).addClass "next"
    if @currentIndex == 0
      @$slideItem.eq(@$itemsCount-1).addClass "left"

ready = ->
  $("[data-role=carousel]").each (i, el) ->
    new Carousel(el)

$(document).on('turbolinks:load', ready)