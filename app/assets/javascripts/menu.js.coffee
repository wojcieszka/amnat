ready = ->
  $item = $("[data-role=main-pages-item]")

  $item.on "mouseover mouseout", ->
    $(this).find(".subpages-wrapper").toggleClass "visible"

  $("#min-menu").on "click", ->
    $(".main-header-navigation").toggleClass "visible"

  if $("li.main-page").find(".subpages-wrapper").length
    $(this).addClass "with-children"

$(document).on('turbolinks:load', ready)