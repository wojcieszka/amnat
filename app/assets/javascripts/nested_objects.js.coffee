class NestedFormObject

  texts:
    remove: ""
    restore: ""
    
  constructor: (el) ->
    @$el = $(el)
    
    @removed = false
    @index = @$el.data("index")

    @$toggleRemoveLink = @$el.find("a[data-role=toggle-remove]")
    @$destroyInput = @$el.find("[data-role=destroy-input]")
    @$inputs = @$el.find("input, select, textarea")

    @texts.remove = @$toggleRemoveLink.attr("title")
    @texts.restore = @$toggleRemoveLink.data("restore-text")
    
    @$toggleRemoveLink.on "click", @handleToggleRemoveClick

  handleToggleRemoveClick: (event) =>
    event.preventDefault()

    @toggleRemove()

  toggleRemove: =>
    @removed = !@removed
    @$el.toggleClass("removed", @removed)
    @$toggleRemoveLink.attr("title", @_text())
    @$toggleRemoveLink.find(".icon").toggleClass("icon-destroy", !@removed).toggleClass("icon-restore", @removed)
    @$inputs.not("[type=hidden]").attr("disabled", @removed)
    console.log @$destroyInput
    @$destroyInput.val(1).attr("disabled", !@removed)

  _text: =>
    if @removed then @texts.restore else @texts.remove

class NestedFormObjects
    
  constructor: (el) ->
    @$el = $(el)
    @$addObject = @$el.find("[data-role=add-object]")
    @$template = @$el.find("[data-role=template]")
    
    
    @index = Math.max(@$el.find("[data-component~=nested-form-object]").map((c) -> $(c).data("attr"))) || 0
    
    @$objectsContainer = @$el.find("[data-role~=objects-container]")
    @$objectsContainer = @$el if @$objectsContainer.length == 0

    @template = _.template(@$template.text().replace(/\{\{([^}]+)\}\}/g, "<%= $1 %>"))
    @$addObject.on "click", @handleAddObjectClick
    
  handleAddObjectClick: (event) =>
    event.preventDefault()
    @addObject()
    
  addObject: =>
    @index++

    objectEl = $.parseHTML(@template(index: @index))
    @$objectsContainer.append(objectEl)
    objectEl = @$el.find("[data-component~=nested-form-object]").last()
    new NestedFormObject(objectEl)
    


ready = ->
    
  $("[data-component=nested-form-objects]").each (i, el) ->
    new NestedFormObjects(el)
    
  $("[data-component=nested-form-object]").each (i, el) ->
    new NestedFormObject(el)

$(document).on('turbolinks:load', ready)