ready = ->
  
  scrollToElement = (id) ->
    $("html, body").animate
      scrollTop: $("#faq-"+id).offset().top,
      "slow"
  
  checkParams = ->
    href = window.location.href
    if href.match(/id/)
      id = href.match(/\d+/)[0]
      scrollToElement(id)
  
  if window.location.pathname == "/faq"
    checkParams()

$(document).on('turbolinks:load', ready)
  