class FaqPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
  end  
    
  def create?
    manage?
  end
  
  private
  
  def user_signed_in?
    user.present?   
  end
  
  def manage?
    admin?
  end

  def admin?
    user_signed_in? && user.admin?
  end
  
end