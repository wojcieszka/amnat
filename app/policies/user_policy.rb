class UserPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
  end  
    
  def create?
    manage?
  end

  def index?
    manage?
  end

  def show_meetings?
    user.id == record.id
  end

  def edit?
    record.id == user.id
  end

  def update?
    record.id == user.id
  end

  def show?
    record.id == user.id
  end
  
  def destroy?
    manage?
  end

  def read?
    record.id == user.id
  end
  
  private
  
  def user_signed_in?
    user.present?   
  end
  
  def manage?
    admin?
  end

  def admin?
    user_signed_in? && user.admin?
  end
  
end