class PagePolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
  end  
    
  def create?
    manage?
  end
  
  def index?
    read?
  end

  def show?
    read?
  end

  def update?
    manage? 
  end

  def destroy?
    manage?
  end
  
  def sort_order?
    manage?
  end
  
  private
  
  def admin?
    user_signed_in? && user.admin?
  end
  
  def user_signed_in?
    user.present?   
  end
  
  def manage?
    admin?
  end
  
  def read?
    user_signed_in?
  end
end