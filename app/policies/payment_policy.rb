class PaymentPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
  end

  def index?
    manage?
  end

  def show?
    manage?
  end

  def create?
    manage?
  end

  def edit?
    manage?
  end

  def destroy?
    manage?
  end

  private

  def manage?
    admin?
  end

  def user_signed_in?
    user.present?
  end

  def admin?
    user_signed_in? && user.admin?
  end

end