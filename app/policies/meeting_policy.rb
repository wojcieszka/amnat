class MeetingPolicy < ApplicationPolicy
  class Scope < Struct.new(:user, :scope)
    def resolve
      scope
    end
  end

  def index?
    manage?
  end

  def create?
    manage?
  end

  def sort_order?
    manage?
  end

  private

  def user_signed_in?
    user.present?
  end

  def manage?
    admin?
  end

  def admin?
    user_signed_in? && user.admin?
  end

  def read?
    user_signed_in?
  end
end