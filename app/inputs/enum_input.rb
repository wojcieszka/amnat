class EnumInput < SimpleForm::Inputs::CollectionSelectInput
  def collection
    @collection ||= object.class.send(attribute_name.to_s.pluralize).keys.map(&:to_sym)
  end
end