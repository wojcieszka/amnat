require "securerandom"

class Wysihtml5Input < SimpleForm::Inputs::TextInput
  def input
    @builder.template.content_tag :div, data: { role: "wysihtml5-input" } do
      toolbar + super
    end
  end
  
  private
  
  def toolbar
    @builder.template.content_tag :div, id: toolbar_id, data: { role: "toolbar" }, class: "toolbar" do
      @builder.template.render "inputs/wysihtml5_input/toolbar"
    end
  end
  
  def toolbar_id
    "toolbar_#{SecureRandom.uuid}"
  end
end