class MeetingService

  attr_accessor :meeting

  def initialize(opts = {})
    @meeting = Meeting.find(opts[:meeting_id]) if opts[:meeting_id].present?
  end

  def create_week_meetings(params = {})
    all_trainings = TrainingDay.all
    all_trainings.each do |training|
      create_meeting(training)
    end

  end

  def create_meeting(training)
    day_name = training.name
    time     = training.time_start.to_time
    event_at = get_event_datetime(day_name, time)

    meeting = Meeting.new(event_at: event_at, training_id: training.id)
    meeting.save
  end

  def get_event_datetime(day_name, time)
    date     = Date.today
    datetime = DateTime.new(date.year, date.month, date.day, time.hour, time.min)
    event_at = datetime.next_week(day_name.to_sym, same_time: true)
  end

  def sign_up(user)
    meeting.users << user
    meeting.save
  rescue => e
    meeting.errors.messages[:user] = I18n.t(e.message)
    return false
  end

  def cancel_user_meeting(user)
    user_meeting = UserMeeting.where(meeting_id: meeting.id, user_id: user.id).first
    return if user_meeting.nil?
    user_meeting.delete
  end

end