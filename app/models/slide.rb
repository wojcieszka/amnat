class Slide < ActiveRecord::Base
  
  mount_uploader :image, SlideImageUploader
  scope :ordered, -> { order(:position) }
end
