class Message < ActiveRecord::Base
  include Humanizer
  require_human_on :create
  before_create :set_status
  
  validates :title, :email, :content, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  
  enum status: [ :fresh, :read ]
  
  scope :ordered, -> { order("created_at DESC") }
  
  private
  
  def set_status
    self.status ||= :fresh
  end
  
end
