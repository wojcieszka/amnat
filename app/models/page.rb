class Page < ActiveRecord::Base
  SPECIAL_PAGE_NAMES = %w(users root contact faqs coaches fighters groups news_posts team trainings passes
    media videos galleries users events partners personal_trainings meetings)
  
  translates :title, :menu_title, :slug
  
  before_create :set_default_position
  
  validates :title, :slug, presence: true
  validates :slug, uniqueness: true, format: { with: /\A[a-z0-9\-]+\z/, message: "Nieprawidłowy fragment adresu URL" }, unless: :special?
  
  has_many :contents, -> { localized }, class_name: "PageContent", dependent: :destroy
  has_many :children, class_name: "Page", foreign_key: "parent_id", dependent: :destroy
  has_one :seo, -> { localized }, as: :seoable, class_name: "Seo"
  belongs_to :parent, class_name: "Page"
  
  enum menu: [ :main, :side ]
  
  accepts_nested_attributes_for :contents
  accepts_nested_attributes_for :seo

  scope :ordered, -> { order("position ASC") }
  scope :in_menu, -> (menu) { where(menu: menus[menu]) }
  scope :root, -> { where(parent_id: nil) }
  
  def special?
    SPECIAL_PAGE_NAMES.include?(name)
  end
  
  def content(name = nil)
    if contents.any?
      contents.select{ |c| c.name == name }.first.content
    end
  end
  
  def contents
    super.build unless super.any?
    super
  end
  
  def main_contents
    contents.select { |c| c.name.blank? }
  end
  
  def seo
    super || build_seo
  end
  
  private
  
  def set_default_position
    self.position ||= 1 + (self.class.maximum(:position) || 0)
  end
  
end
