module Localized
  extend ActiveSupport::Concern

  included do
    scope :localized, -> (locale = I18n.locale) { where(locale: locale) }
    before_create :set_default_locale
  end

  private

  def set_default_locale
    self.locale ||= I18n.locale
  end
end
