class UserMeeting < ActiveRecord::Base
  belongs_to :user, foreign_key: :user_id
  belongs_to :meeting, foreign_key: :meeting_id

  validates_uniqueness_of :user_id, scope: :meeting_id, message: I18n.t("meeting.errors.you_are_already_signed_up")
end
