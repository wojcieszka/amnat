class Member < ActiveRecord::Base
  
  before_create :generate_token
  
  scope :ordered, -> { order(:last_name) }

  has_many :payments
  
  enum status: [ :unpaid, :paid ]
  
  def name
    first_name + " " +last_name
  end

  def group
    return unless payments.any?
    payments.ordered.first.try(:group)
  end
  
  private
  
  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless self.class.exists?(token: random_token)
    end
  end

end
