class Coach < ActiveRecord::Base
  translates :description
  
  mount_uploader :image, CoachImageUploader
  
end
