class Faq < ActiveRecord::Base
  
  scope :ordered, -> { order("question ASC") }
end
