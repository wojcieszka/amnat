class NewsPost < ActiveRecord::Base
  
  has_many :comments, dependent: :destroy
  
  mount_uploader :image, NewsImageUploader
  
  scope :ordered, -> { order("created_at DESC") }
  
end
