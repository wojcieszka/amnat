class Event < ActiveRecord::Base
  extend FriendlyId
  extend SimpleCalendar
  # has_calendar
  
  friendly_id :title, use: :slugged
  mount_uploader :image, EventImageUploader
    
  scope :future, -> { where("starts_at >= ?", Date.today)}
  scope :ordered, -> { order("starts_at ASC") }

  def start_time
    self.starts_at
  end
end
