class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :set_role

  has_many :user_meetings
  has_many :meetings, through: :user_meetings

  validates :first_name, :last_name, presence: true

  scope :sign_up_list, -> { includes(:user_meetings).order('user_meetings.created_at') }

  def admin?
    role == "admin"
  end

  def set_role
    self.role = "ordinary"
  end
end
