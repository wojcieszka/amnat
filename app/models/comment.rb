class Comment < ActiveRecord::Base
  belongs_to :news_post
  include Humanizer
  require_human_on :create
  
  validates :author, :content, presence: true
  scope :ordered, -> { order("created_at DESC") }
  
end
