class Gallery < ActiveRecord::Base
  
  has_many :images, class_name: "GalleryImage", dependent: :destroy
  before_create :set_default_position
  
  accepts_nested_attributes_for :images, allow_destroy: true
  
  scope :ordered, -> { order("position ASC") }
  
  private
  
  def set_default_position
    self.position ||= 1 + (self.class.maximum(:position) || 0)
  end
  
end
