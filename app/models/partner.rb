class Partner < ActiveRecord::Base
  
  mount_uploader :image, PartnerImageUploader
  
end
