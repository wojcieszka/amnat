class TrainingDay < ActiveRecord::Base
  belongs_to :group
  has_many :meetings, foreign_key: :training_id

  DAY_NAMES = %w(monday tuesday wednesday thursday friday)
  
end
