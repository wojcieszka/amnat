class Meeting < ActiveRecord::Base
  MAX_USERS_COUNT = 20
  RESERVE_USERS_COUNT = 2
  belongs_to :training, class_name: "TrainingDay", foreign_key: :training_id, dependent: :destroy
  has_one :group, through: :training, dependent: :destroy
  has_many :user_meetings
  has_many :users, through: :user_meetings

  validates :event_at, uniqueness: { scope: :training_id }

  scope :active, -> { where("event_at > ?", DateTime.now).order(event_at: :asc) }

  def open?
    users.count < MAX_USERS_COUNT + RESERVE_USERS_COUNT
  end

  def self.first_active_date
    self.active.first
  end

  def self.last_active_date
    self.active.last
  end

end
