class Seo < ActiveRecord::Base
  include Localized
  
  DEFAULT_TITLE = "Am Nat"
  DEFAULT_AUTHOR = "Anna Wojcieszek"
  belongs_to :seoable, polymorphic: true
  
  attr_reader :image
  
  def og_type
    "object"
  end
  
end
