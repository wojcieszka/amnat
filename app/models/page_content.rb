class PageContent < ActiveRecord::Base
  include Localized
  
  belongs_to :page
  
  scope :with_name, -> { where("name IS NOT NULL") }
end
