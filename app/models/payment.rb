class Payment < ActiveRecord::Base

  belongs_to :member
  belongs_to :group
  scope :ordered, -> { order("created_at DESC") }

end
