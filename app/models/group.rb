class Group < ActiveRecord::Base
  has_many :trainings, class_name: "TrainingDay", dependent: :destroy
  has_many :meetings, through: :trainings

  before_create :set_default_position

  accepts_nested_attributes_for :trainings, allow_destroy: true
  LEVELS = %w(beginner advanced middle mixed youth)

  scope :ordered, -> { order("position ASC") }

  private

  def set_default_position
    self.position ||= 1 + (self.class.maximum(:position) || 0)
  end

end
