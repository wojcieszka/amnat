module AdminHelper
  def admin_menu(params = {}, &block)
    content = capture &block
    return '' if content.blank?
    css_class = "admin"
    css_class += params[:inline] ? " inline" : " dropdown"
    content = content_tag "menu", content, class: css_class
    if params[:inline]
      content
    else
      content_tag :div, class: "menu-dropdown-wrapper" do
        (content_tag :i, "", class: "yellow-gear icon") + content
      end
    end
  end
  
  def admin_link_to(type, text, url, params = {})
    params[:class] ||= ""
    params[:class] += " #{type}"
    content_tag :li, link_to(text, url, params)
  end
  
end