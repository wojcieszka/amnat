module MetaTagsHelper
  def title(string)
    titles << string unless string.blank?
    string
  end

  def page_title(base = nil)
    title(@seo.title) if @seo
    [*titles, base].compact.join(" - ").html_safe
  end

  def current_title
    titles.last
  end
  
  def seo_tags
    SeoDecorator.decorate(@seo).meta_tags if @seo
  end
  
  private

  def titles
    @titles ||= []
  end
  
end
