module PriceHelper
  
  def price_format(price)
    number_to_currency(price, unit: "zł", precision: 0, format: "%n %u")
  end

  def personal_training_price(price)
    price_format(price) + " / 60 min"
  end
  
end