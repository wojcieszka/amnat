module ApplicationHelper
  
  def flash_message
    result = []
    flash.each do |type, message|
      if message.present?
        content = content_tag(:span, message, class: "content")
        content += link_to "", "", class: "close", data: { role: "close-flash" }
        result << content_tag(:div, content, class: ["flash-message", type], data: { role: "flash-content" })
      end
    end
    result.join.html_safe
  end
  
  def short_home_description(content)
    truncate(content, length: 200)
  end

  def days_collection_for_select
    TrainingDay::DAY_NAMES
  end

  def group_levels_collection_for_select
    Group::LEVELS
  end

  def position_class(index)
    "reservation" if index > Meeting::MAX_USERS_COUNT
  end

  def format_meeting_date(meeting)
    return if meeting.nil?
    meeting.event_at.utc.strftime("%d-%m-%Y")
  end
  
end
