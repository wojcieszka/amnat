module StatusHelper
  
  def status_label(status)
    if status == "unpaid"
      content_tag :span, status, class: "label label-important"
    else
      content_tag :span, status, class: "label label-success"
    end
  end
  
end