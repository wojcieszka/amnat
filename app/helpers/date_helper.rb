module DateHelper
  
  def short_date(date)
    l date, format: ("%d %B %Y")
  end
  
end