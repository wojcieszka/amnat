module PageHelper
  
  def pages_in_menu(menu, level = 0)
    scope = if level.zero?
      @pages.root
    else
      selected_pages[level-1].children
    end
    
    PageDecorator.decorate_collection(scope.ordered.in_menu(menu), context: { current_page: @page })
  end
  
  def selected_pages
    @selected_pages ||= [@page.try(:parent), @page].compact
  end
  
  def menu_link_to page
    link_to page.menu_title, friendly_page_path(page)
  end
  
end
